﻿using UnityEngine;
using System.Collections;

public class TreadmillInitialization : MonoBehaviour {

	public ColliderInitialization mCollider;
	public Transform bowlCentre;

	// Use this for initialization
	void Start () {
		ResetOrientation ();
	}

	public void ResetOrientation(){

		//Retrieve saved treadmill 
		TreadmillSettings temp = TreadmillSettings.Load();

		// Move and rotate treadmill based on saved values
		Vector3 savedPosition = new Vector3(temp.xPosition, temp.yPosition, temp.zPosition);
		Vector3 savedRotation = Vector3.up * temp.rotation;

		transform.position = savedPosition;
		transform.rotation = Quaternion.Euler(savedRotation);

		Vector3 colliderRot = transform.rotation.eulerAngles;
		colliderRot.y += 180f;
		mCollider.CorrectPosition (transform.localPosition, Quaternion.Euler(colliderRot));
	}
	
	
}
