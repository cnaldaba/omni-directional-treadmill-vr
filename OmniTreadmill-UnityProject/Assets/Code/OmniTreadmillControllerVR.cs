﻿using UnityEngine;
using System.Collections;


/// <summary>
/// The omni-directional controller that moves a player
/// </summary>

public class OmniTreadmillControllerVR : PCPlayerController
{

	public OmniTreadmillTracker omniTreadmill;
	public HeadingReset mHeading;
	public HmdLock HMDCameraRigCorrection;
	public Transform headCamera;
	public Transform heading;
	public Transform structure;

	private Rigidbody mRbody;

	private float remainTime;

	private bool startedWalk;
	private Vector3 walkDirection;

	private float omniTreadmill_sensitivityY;

	// Use this for initialization
	void Start ()
	{
		base.Start ();
		this.isDecoupled = true;

		mRbody = GetComponent<Rigidbody> ();
		startedWalk = false;

		VRNControls temp = VRNControls.Load ();

		omniTreadmill_sensitivityY = temp.stickSensitivityY;
	}


	protected override void ChangeDecoupled (bool isDecoupled)
	{
		base.ChangeDecoupled (isDecoupled);
	}

	protected override float GetForwardMotion ()
	{
		float motionY = 0f;

		if (omniTreadmill != null) {
			motionY = omniTreadmill_sensitivityY * omniTreadmill.velocity;
		}


		//keyboard input overrides joystick
		float verticalInput = keySensitivityFwdBwd * Input.GetAxis ("Vertical");
		if (verticalInput != 0f) {
			motionY = verticalInput;
		}

		return motionY;
	}

	protected override float GetRightMotion ()
	{
		if (omniTreadmill != null) {
			float right = 2.0f * omniTreadmill.velocity * omniTreadmill.direction.x;
			return right;

		}
		return 0f;
	}


	protected override float GetRotation ()
	{
		return -1.0f * base.GetRotation ();
	}

	void Update ()
	{
   
		if (Input.GetButtonDown ("Toggle_Decoupled_Mode")) {
			this.isDecoupled = !this.isDecoupled;
			HMDReset ();
		}
		// Reset rotations to kill drift
		if (Input.GetButtonDown ("OVR Reset")) {
			Debug.Log ("OVR Reset");
			HMDReset ();
		}

		forwardMotion = Time.deltaTime * GetForwardMotion ();
		rightMotion = Time.deltaTime * GetRightMotion ();
		rotationAngle = Time.deltaTime * GetRotation ();

		if (!this.isDecoupled && !this.isParalyzed) {
  

			Vector3 translationVector = new Vector3 (0f, 0f, forwardMotion);
			transform.Translate (translationVector);

			Vector3 rotationVector = new Vector3 (0, rotationAngle, 0);
			transform.Rotate (rotationVector);

			Vector3 inverseVector = new Vector3 (0, -rotationAngle, 0);
			HMDCameraRigCorrection.transform.Rotate (inverseVector);

			heading.transform.Rotate (rotationVector);

		}

      
	}

	void HMDReset ()
	{

		// find head global rotation
		Quaternion headOldLocalRotation = headCamera.localRotation;
		Quaternion headOldRotation = HMDCameraRigCorrection.transform.rotation * headOldLocalRotation;

		// find global yaw for PlayerController
		float playerControllerYaw = this.transform.rotation.eulerAngles.y;

		// rotate PlayerController so it lines up with head global rotation, then set HMDCameraRigCorrection to 0
		float rotationCorrection = headOldRotation.eulerAngles.y - playerControllerYaw; // rotation of head relative to body
		Debug.Log ("Rotating " + rotationCorrection + " degrees in Space.Self");
		transform.Rotate (0f, rotationCorrection, 0f, Space.Self);

		HMDCameraRigCorrection.ResetCorrection (); // person is looking straight ahead, so align their body with their gaze so as to not break immersion by snapping the camera around
		mHeading.ResetCorrection ();
	

	}

}

