﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Tracks a foot to determine gait phase, velocity and direction
/// </summary>

public class OmniTreadmillFeet : MonoBehaviour
{
    public enum WALKING_PHASES
    {
        STANCE,
        SWING
    }

    public float speed;
    private Vector3 lastPosition;
    public bool onTreadmill, walking;
    public Text speedText, statusText;
    private string name;

    public Transform centrePoint;

	// For average filter:
    private List<float> distAvgFilt;
    private float[] peakWindow;
    private float[] zeroXWindow;
    private Vector3[] pointsForDirection;

	// For velocity calculation:
    public float dist2, dist1;
    public float velocity;
    public Vector3 direction, heading;

	// Based on signal analysis
    private const int FILTSIZE = 25;
    private const float PEAKTHRESHOLD = 0.35f;

    public WALKING_PHASES phase;
    // Use this for initialization
    void Start()
    {
        lastPosition = this.transform.position;
        name = this.transform.name;
        walking = false;

        Debug.Log("Centre point is " + "x: " + centrePoint.position.x + ", " + "y: " + centrePoint.position.y + ", " + "z: " + centrePoint.position.z + ", ");

        //Initialize average filter list
        distAvgFilt = new List<float>();

        peakWindow = new float[4];
        peakWindow[0] = 0f;
        peakWindow[1] = 0f;
        peakWindow[2] = 0f;
        peakWindow[3] = 0f;

        zeroXWindow = new float[2];
        zeroXWindow[0] = 0f;
        zeroXWindow[1] = 0f;

        pointsForDirection = new Vector3[2];
        pointsForDirection[0] = Vector3.zero;
        pointsForDirection[1] = Vector3.zero;

        phase = WALKING_PHASES.STANCE;
        dist2 = 0f;
        statusText.text =  name + "Stance";
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(this.transform.position, centrePoint.position);

        distAvgFilt.Add(distance);

        // Filter the distance
        dist1 = distAvgFilt.Sum() / (float) FILTSIZE;
        speedText.text = name + " Dist " + dist1;

        // find peaks
        peakWindow[3] = peakWindow[2];
        peakWindow[2] = peakWindow[1];
        peakWindow[1] = peakWindow[0];
        peakWindow[0] = dist1;

        //Calculate velocity
        velocity = (dist2 - dist1) / Time.deltaTime;

        //Calculate zero crossing
        zeroXWindow[1] = zeroXWindow[0];
        zeroXWindow[0] = velocity;

        //Determine direction
        pointsForDirection[1] = pointsForDirection[0]; // Last Position
        pointsForDirection[0] = this.transform.position; // Current Position
        heading = pointsForDirection[1] - pointsForDirection[0]; // Last - Current
        heading.y = 0;
        heading *= 10000f; // to ensure that magnitude is < 1
        direction = heading / heading.magnitude;


        switch (phase)
        {
            case WALKING_PHASES.SWING:
                 bool peak1 = (peakWindow[0] < peakWindow[1]) && (peakWindow[1] > peakWindow[2]) && (peakWindow[1] > PEAKTHRESHOLD);
                 bool peak2 = (peakWindow[0] < peakWindow[1]) && (peakWindow[1] == peakWindow[2]) && (peakWindow[2] > peakWindow[3]) && (peakWindow[1] > PEAKTHRESHOLD);
                 if (peak1 || peak2)
                 {
                     phase = WALKING_PHASES.STANCE;
                     statusText.text = name + " Stance";
                 }
                 break;
            case WALKING_PHASES.STANCE:
                 if ((zeroXWindow[1] > 0) && (0>zeroXWindow[0]))
                {
                    phase = WALKING_PHASES.SWING;
                     statusText.text = name  + " Swing";
                }
                break;
            default:
                break;
        }


      
    

        dist2 = dist1;
        if (distAvgFilt.Count == FILTSIZE) // Remove the element before next iteration
        {
            distAvgFilt.RemoveAt(0);
        }
  

    }


    
}
