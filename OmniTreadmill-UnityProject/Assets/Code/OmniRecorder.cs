﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using UnityEngine.UI;

/// <summary>
/// Utilized to create omni-directional treadmill algorithm
/// </summary>

public class OmniRecorder : MonoBehaviour
{

	bool recording;
	private static string fileNameHeader = "OMNI_recordingV2";
	public Transform rightFoot, leftFoot;
	private string outputPath;
	private string filePathAll;
	public OmniTreadmillFeet rightFootTrack, leftFootTrack;
	public Text recordStatusText;

	// Use this for initialization
	void Start ()
	{
		outputPath = Application.dataPath;
		Debug.Log ("filePath " + outputPath);
		rightFootTrack = rightFoot.GetComponent<OmniTreadmillFeet> ();
		leftFootTrack = leftFoot.GetComponent<OmniTreadmillFeet> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetButtonDown ("Toggle_Record_Mode")) {
			if (!recording) {
				recording = true;
				RecordHeaderLine (Application.dataPath);
				if (recordStatusText != null) {
					recordStatusText.text = "Recording";
				}
			} else {
				recording = false;
				if (recordStatusText != null) {
					recordStatusText.text = "NOT Recording";
				}
			}
		}


		if (recording) {
			RecordData (GetDataLine ());
		}
	
	}

	private void RecordHeaderLine (string outputFilePath)
	{
		string header = "delta time,Rdist,Rvel,Ldist, Lvel";
		// Delta time, Right foot distance, Right foot velocity, Left foot distance, Left foot velocity;
		// Note the position is the distance away from the centre of omni-directional treadmill's concave bowl
		string dateFormat = DateTime.Now.ToString ("_yy-MM-dd_HH-mm");

		string correspondingFileName = fileNameHeader + dateFormat;
		FileStream mfileStream = DataLogger.OpenFileStream (outputFilePath, correspondingFileName, "csv", 0, DataLogger.FileCollisionHandlerMode.NONE);
		StreamWriter fileOutput = new StreamWriter (mfileStream);

		filePathAll = mfileStream.Name;
		fileOutput.WriteLine (header);
		fileOutput.Close ();

		Debug.Log ("Wrote file to " + outputFilePath);


	}

	private void RecordData (string dataLine)
	{
		if (filePathAll != null) {
			StreamWriter fileOutput = new StreamWriter (filePathAll, true);
			fileOutput.WriteLine (dataLine);
			fileOutput.Close ();
		}
	}

	private string GetDataLine ()
	{
		string data = "";
		if ((rightFoot != null) && (leftFoot != null)) {
			data = string.Format ("{0:F4},{1:F4},{2:F4},{3:F4},{4:F4}",
				Time.deltaTime,
				rightFootTrack.dist1,
				rightFootTrack.velocity,
				leftFootTrack.dist1,
				leftFootTrack.velocity);
		}
     

		return data;
	}
}
