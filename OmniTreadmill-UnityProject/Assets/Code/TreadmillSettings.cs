﻿using System;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

/// <summary>
/// Saves omni-directional treadmill settings as an .xml file
/// Ensures SteamVR play space and omni-directional treadmill's concave bowl are aligned
/// </summary>

[XmlRoot("TreadmillSettings")]
public class TreadmillSettings : AbstractVRNSettings
{
	/// <summary>
	/// Settings file name
	/// </summary>
    public const string TREADMILL_SETTINGS_FILE_NAME = "TreadmillSettings.xml";


    /// <summary>
    /// X Position
    /// </summary>
    [XmlElement("XPosition")]
    public float xPosition;

    /// <summary>
    /// Y Position
    /// </summary>
    [XmlElement("YPosition")]
    public float yPosition;

    /// <summary>
    /// Z Position
    /// </summary>
    [XmlElement("ZPosition")]
    public float zPosition;


    /// <summary>
    /// Rotation
    /// </summary>
    [XmlElement("Rotation")]
    public float rotation;


    private static TreadmillSettings theInstance = null;

    public static TreadmillSettings Load()
    {
        string settingsPath = Application.persistentDataPath + "/" + TREADMILL_SETTINGS_FILE_NAME;
        try
        {
            theInstance = (TreadmillSettings)AbstractVRNSettings.Load(settingsPath, typeof(TreadmillSettings), theInstance);
            return theInstance;
        }
        catch (InvalidOperationException ex)
        {
            Debug.LogError("could not parse the file at " + settingsPath + ". creating a new one." + ex);

            DateTime dateTime = DateTime.Now;
            string dateString = dateTime.ToString("yyyy_MMM_dd_HH_mm_ss");

            // back up corrupt file (for postmortem)
            File.Copy(settingsPath, settingsPath + "_corrupt_" + dateString);
            File.Delete(settingsPath);

            // try again
            theInstance = (TreadmillSettings)AbstractVRNSettings.Load(settingsPath, typeof(TreadmillSettings), theInstance);
            return theInstance;
        }
    }

    public override void Save()
    {
        base.Save(Application.persistentDataPath, TREADMILL_SETTINGS_FILE_NAME, typeof(TreadmillSettings));
    }

    public TreadmillSettings()
    {
        this.xPosition = 0.0f;
        this.yPosition = 0.0f;
        this.zPosition = 0.0f;
        this.rotation = 0.0f;
    }
}
