﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Initializes user's collider to be in the centre of omni-directional treadmill's concave bowl
/// </summary>
public class ColliderInitialization : MonoBehaviour {

	public void CorrectPosition(Vector3 position, Quaternion rotation){
		CapsuleCollider mCollider = GetComponent<CapsuleCollider> ();

		Vector3 newPosition = rotation * position;
		newPosition.y = mCollider.center.y;
		mCollider.center = newPosition;

	}

}
