﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Tracks left and right foot to determine user's overall gait phase, velocity and direction
/// </summary>

public class OmniTreadmillTracker : MonoBehaviour
{

	public float averageSpeed;
	public Quaternion orientation;
	public OmniTreadmillFeet leftFoot, rightFoot;
	public Text speedText;
	public Vector3 direction;


   
	public enum WALKING_PHASES
	{
		SWING,
		STANCE_FOOT_RIGHT,
		STANCE_FOOT_LEFT,
		STANCE_BOTH // shouldn't happen
	}

	public WALKING_PHASES phase;
	public float velocity, lastVel, currentVel;

	private Transform mParent;
	private Rigidbody rbParent;

	// Use this for initialization
	void Start ()
	{
		averageSpeed = 0f;

		phase = WALKING_PHASES.SWING;

		lastVel = 0f;
		currentVel = 0f;
		mParent = this.transform.parent;

		rbParent = mParent.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame, what gait phase is user in now?
	void Update ()
	{

		if ((leftFoot != null) && (rightFoot != null)) {

			switch (phase) {
			case WALKING_PHASES.SWING:
				if ((rightFoot.phase == OmniTreadmillFeet.WALKING_PHASES.STANCE) && (leftFoot.phase == OmniTreadmillFeet.WALKING_PHASES.SWING)) {
					phase = WALKING_PHASES.STANCE_FOOT_RIGHT;
				} else if ((rightFoot.phase == OmniTreadmillFeet.WALKING_PHASES.SWING) && (leftFoot.phase == OmniTreadmillFeet.WALKING_PHASES.STANCE)) {
					phase = WALKING_PHASES.STANCE_FOOT_LEFT;
				}
				velocity = 0f;
				currentVel = 0f;
				direction = Vector3.zero;
				break;
                    
			case WALKING_PHASES.STANCE_FOOT_RIGHT:
				if ((rightFoot.phase == OmniTreadmillFeet.WALKING_PHASES.SWING) && (leftFoot.phase == OmniTreadmillFeet.WALKING_PHASES.SWING)) {
					phase = WALKING_PHASES.SWING;
				} else if ((rightFoot.phase == OmniTreadmillFeet.WALKING_PHASES.SWING) && (leftFoot.phase == OmniTreadmillFeet.WALKING_PHASES.STANCE)) {
					phase = WALKING_PHASES.STANCE_FOOT_LEFT;
				}
				velocity = rightFoot.velocity;
				currentVel = rightFoot.velocity;
				direction = rightFoot.direction;
				break;
			case WALKING_PHASES.STANCE_FOOT_LEFT:
				if ((rightFoot.phase == OmniTreadmillFeet.WALKING_PHASES.STANCE) && (leftFoot.phase == OmniTreadmillFeet.WALKING_PHASES.SWING)) {
					phase = WALKING_PHASES.STANCE_FOOT_RIGHT;
				} else if ((rightFoot.phase == OmniTreadmillFeet.WALKING_PHASES.SWING) && (leftFoot.phase == OmniTreadmillFeet.WALKING_PHASES.SWING)) {
					phase = WALKING_PHASES.SWING;
				}
				velocity = leftFoot.velocity;
				currentVel = leftFoot.velocity;
				direction = leftFoot.direction;
				break;
			default:
				break;
			}


			// Move user forward!
			float acceleration = (currentVel - lastVel) / Time.deltaTime;
			Vector3 force = new Vector3 (acceleration * direction.x, 0f, acceleration * direction.z);
			lastVel = currentVel;


 
		}
	}
}
