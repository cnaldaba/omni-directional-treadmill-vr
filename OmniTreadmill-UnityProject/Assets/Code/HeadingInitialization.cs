﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Initializes forward heading direction (arrow)
/// </summary>

public class HeadingInitialization : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Debug.Log ("Heading init");
		//Retrieve saved treadmill 
		TreadmillSettings temp = TreadmillSettings.Load();

		// Move and rotate treadmill based on saved values
		Vector3 savedPosition = new Vector3(temp.xPosition, temp.yPosition, temp.zPosition);
		Vector3 savedRotation = Vector3.up * temp.rotation;

		this.transform.position = savedPosition;
	}
	

}
