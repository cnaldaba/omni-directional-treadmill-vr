﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Calibrates omni-directional treadmill to be in the centre of
/// SteamVR's play space
/// </summary>
public class calibrateOmniTreadmillPosition : MonoBehaviour
{
    private float step = 0.005f;
    private float rotateStep = 1.0f;

    GameObject parent;


    // Use this for initialization
    void Start()
    {
      
        //Retrieve saved treadmill 
        TreadmillSettings temp = TreadmillSettings.Load();

        // Move and rotate treadmill based on saved values
        Vector3 savedPosition = new Vector3(temp.xPosition, temp.yPosition, temp.zPosition);
        Vector3 savedRotation = Vector3.up * temp.rotation;

        transform.position = savedPosition;
        transform.rotation = Quaternion.Euler(savedRotation);

        parent = transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Alpha0))
        {

            Debug.Log("SAVED FOLLOWING VALUES:");
            Debug.Log("Position x: " + transform.position.x + ", y: " + transform.position.y + ", z: " + transform.position.z);
            Debug.Log("Rotation: " + transform.rotation.eulerAngles.y);

            TreadmillSettings temp = TreadmillSettings.Load();

            temp.xPosition = transform.position.x;
            temp.yPosition = transform.position.y;
            temp.zPosition = transform.position.z;

            temp.rotation = transform.rotation.eulerAngles.y;
            temp.Save();
            parent.GetComponent<PlayerController>().ShowPopup("Calibrated!", "Saved treadmill settings.", null, 3000);
        
        }

        if (Input.GetKeyDown(KeyCode.Keypad3)) // Right
        {
            transform.Translate(Vector3.right * step);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad1)) // Left
        {
            transform.Translate(Vector3.left * step);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad2)) // backward
        {
            transform.Translate(Vector3.back * step);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad5)) // forward
        {
            transform.Translate(Vector3.forward * step);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad6)) // up
        {
            transform.Translate(Vector3.up * step);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad4)) // down
        {
            transform.Translate(Vector3.down * step);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad7)) //counter clockwise rotation
        {
            transform.Rotate(Vector3.up * rotateStep, Space.World);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad9)) // clockwise rotation
        {
            transform.Rotate(-1.0f * Vector3.up * rotateStep, Space.World);
        }



    }
}
