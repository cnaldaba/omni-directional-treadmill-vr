﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Resets forward heading direction if becomes physical and virtual heading becomes unaligned
/// </summary>

public class HeadingReset : MonoBehaviour {

	public void ResetCorrection()
	{
		this.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

		this.transform.localRotation = Quaternion.identity;
	}
}
