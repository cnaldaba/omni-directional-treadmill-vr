# Omni-directional Treadmill Project (Code Snippet)
Project description:
An omni-directional treadmill utilized as a VR controller with an HTC Vive and SteamVR in the Unity game engine. 
This does not include the entire code utilized in our [virtual reality navigation research](http://bme.ee.umanitoba.ca/research/neurological/virtual-reality/).

Programming language: C#
Software used: Unity 5.3.2f1, 3DS Max

Utilization: Master of science thesis project that investigates different VR locomotive controllers and their effects on user simulator sickness and controller intuitiveness.

Publications:
C. N. Aldaba, P. J. White, A. Byagowi and Z. Moussavi, "Virtual reality body motion induced navigational controllers and their effects on simulator sickness and pathfinding," 2017 39th Annual International Conference of the IEEE Engineering in Medicine and Biology Society (EMBC), Seogwipo, 2017, pp. 4175-4178 [[url]](https://ieeexplore.ieee.org/document/8037776)

C. N. Aldaba, "Investigation of virtual reality locomotion technology effects on simulator sickness and application for neuro-cogntive training for participants with memory problems" M. Sc Thesis, Dept. Biomedical Eng., University of Manitoba, Winnipeg, Canada, 2018 [[url]](http://hdl.handle.net/1993/33301)
